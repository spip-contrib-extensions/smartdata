# Une boucle (SMARTDATA)

La boucle SMARTDATA est une variante de la boucle DATA qui apporte à l'itérateur la connaissance des critères de la boucle qui l'appelle.
Au plus près des données, on peut alors affiner le calcul du tableau de donnée renvoyer, et notamment on peut tenir compte des critères qui en filtrent le contenu. 
C'est pour cela qu'elle est "smart" : elle peut tenir compte des critères  pour déployer des algorythmes complexes de filtrage ou de calcul des données.

Ça permet par exemple de ne pas calculer et renvoyer un tableau de 10000 items 
si il y a un critère {6,2} qui ne demande que 2 items, et qu'on a pas par ailleurs besoin du reste des items.

Mais ça permet aussi plein d'autres ajustements.

La boucle SMARTDATA est une surcouche de la boucle DATA, et utilise la plupart de son code.

# Implémentation d'une boucle SMARTDATA

Pour implémenter ce nouveau type de boucle (SMARTDATA) sur une source de type `format`,
au lieu de définir une fonction `inc_format_to_array_dist` comme il faut le faire pour une boucle (DATA)
il faut définir une fonction `inc_format_to_smart_array_dist`

Exemple d'usage :
```
<H2>BOUCLE DATA</H2>
<BOUCLE_data(DATA){source micmac,test}{datacache 0}{3,5}>
[#CLE] #VALEUR<br>
</BOUCLE_data>

<H3>BOUCLE SMARTDATA</H3>
<BOUCLE_smartdata(SMARTDATA){source micmac,test}{3,5}{datacache 0}>
[#CLE] #VALEUR<br>
</BOUCLE_smartdata>
```

- Avec la boucle DATA data : le tableau complet (10000 éléments) est calculé par l'itérateur  inc_micmac_to_array_dist, et c'est les critères communs des boucles SPIP qui ne retiennent que les items satisfaisant le critère {3,5}
- Avec la boucle SMARTDATA : seul le tableau des éléments nécessaires est calculé par l'itérateur inc_micmac_to_smart_array_dist. Seul le tableau des 5 items demandés est calculé.

# Fonctionnement

- au lieu d'une fonction `inc_format_to_array_dist`, c'est une fonction `inc_format_to_smart_array_dist` qu'il faut définir
- cette fonction reçoit en premier argument les critères "communs" de la boucle SPIP, suivi des arguments passé à la boucle dans le critère source (champ 'command' de la structure Boucle)
- elle peut s'en servir pour ne construire que ce qui est nécessaire dans le tableau. 
> Par exemple : avec le critère `limit` produit par le critère `{a,b}`, elle ne produit et renvoie alors qu'un tableau de b éléments à partir du a.
- elle actualise les critères en conséquence et les renvoie à la boucle SPIP, pour que celle-ci gère les critères qui n'ont pas encore été traités.

### Implémentation d'un format
 
La fonction `inc_format_to_smart_array_dist` peut être définie sans surprise dans un fichier `inc/format_to_smart_array.php`, 
mais elle peut aussi être définie dans `inc/format_to_array.php`, c'est à dire le fichier employé pour définir un format de boucle DATA.

On peut ainsi définir dans le même fichier la version smart (qui servira avec les boucles SMARTDATA) et l'autre (qui servira avec les boucles DATA)

Tous les critères ne sont pas reçus par la fonction : c'est le champ 'command' de la structure BOUCLE qui est transmis.

Rq : Dans la classe IterateurDATA il y a une variable membre `$filtre` qui ne sert à rien mais dont le PhpDoc indique : « * Dans la classe IterateurDATA il y a une variable membre `$filtre` dont le PhpDoc indique : « Conditions de filtrage ie criteres de selection ». 
> Ce champ est utilisé pour `echo "!!!!!!!! <h1>filtres=$filtres</h1>";` ce qui indique un usage peu fréquent. 
> Était elle prévue pour un traitement des filtres comme ce que fait SMARTDATA ? 

### Conséquence pour #CLE
- avec DATA, les clés des items parcourus par une boucle avec le critère `{3,5}` vont de 3 à 7.
- avec SMARTDATA et toujours le critère `{3,5}` ces clés vont de 0 à 4. 

### Limites : le datacache

Avec les boucles DATA, le résultat des fonctions `_to_array` appelées est mis en datacache. Donc la fonction `_to_array` n'est appelée qu'une seule fois lorsqu'on veut `{0,10}` puis `{1000,1010}` (à vérifier)

Avec une boucle SMARTDATA qui gère les limites `{x,y}`, la fonction `_to_smart_array` doit être appelée à chaque fois que les limites changent.
- TODO : Vérifier que le datacache ne courtcircuite pas l'appel à la fonction _to_smart_array. Sinon il faudrait complexifier la gestion du datacache (changer le hash), ou ne pas en avoir du tout pour être smart.

# Alternatives 

On peut préférer garder les boucles DATA : dans ce cas et lorsque le source le prévoit, les critères dont on souhaite qu'ils soient pris en compte par une fonction `_to_array` 
doivent être passés en argument du source. Leur nombre est limité à max 3 (ou 4, je sais plus).
> Par exemple : Au lieu d'un critère SPIP `<BOUCLE_madata(SMARTDATA){source enormeressource}{2,10}>`
> on pourra choisir une syntaxe comme ça : `<BOUCLE_madata(DATA){source enormeressource,2,10}>`
> ou bien `<BOUCLE_madata(DATA){source enormeressource,pages 3-10}>`

Il faut toutefois vérifier, dans ce cas, comment se comporte le datacache. Intègre t il bien les arguments supplémentaires dans son hash ?
S'ils ne sont pas intégrés, il restera la possibilité d'écrire sans argument de source, mais comme des arguments d'url, même si c'est pas une url : 
> `<BOUCLE_madata(DATA){source enormeressource?limit=3-10}>`... sans virgule surtout !

Dans ces différents cas, c'est l'itérateur qui gère `enormeressource` qui doit être adapté pour gérer la syntaxe retenue.
 
Par ailleurs, on peut aussi définir une nouvelle boucle directement à partir de Iterator, de même que IterateurDATA.  

# Conclusions

- SMARTDATA marche bien tel que décrit, à l'intérieur des limites énoncées.

- ç'a été trés agréable à développer grâce à l'usage de "programmation objet" dans SPIP pour les itérateurs 

# Perspectives 

* Continuer le dev en paramétrant le fonctionnement du datacache avec les besoins spécifique de SMARTDATA 

* **Utiliser un nouveau critère** pour choisir un fonctionnement 'smart' ou pas. (Plutôt que d'être smart prioritairement)
> En effet, selon les cas ce sera plus efficace 
>  * d'appeler une seule fois _to_array et de tout ramener systématiquement pour "juste au cas où il y en aurait besoin" 
>  * OU BIEN d'appeler _to_smart_array à chaque fois qu'il y a besoin en ne ramenant que le strict nécessaire.

* **SPIP core** : intégrer le fonctionnement de SMARTDATA dans les boucles DATA ?
> L'itérateur pour SMARTDATA est une sous-classe de l'itérateur pour DATA et il y a juste une quarantaine de lignes de code spécifique. 
> Ce code est conçu pour pouvoir être intégré dans le code de la boucle DATA sans différence et sans coût pour les boucles DATA classiques. 
> C'est à dire qu'il pourrait y avoir une syntaxe unique (DATA) pour tous les formats, smarts ou non, et 
>  - si la fonction `inc_format_to_smart_array_dist` existe, c'est le fonctionnement smart qui est appliqué, 
>  - sinon, c'est le fonctionnement habituel. C'est dans cette éventualité et pour ne pas pénaliser les boucles (DATA) classiques, qu'il est possible de définir la fonction smart dans le fichier `inc/format_to_array.php` qui sert pour définir les boucles DATA historiques.

* Assurer le formalisme exotique SMARTDATA:source (équivalent de DATA:source). (via `requeteur_smartdata`)
