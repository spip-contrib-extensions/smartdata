<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'smartdata_description' => 'Cette nouvelle version des boucles DATA régle un certain nombre de problème des boucles DATA et ajoute de nouvelle fonctionnalités.',
	'rainette_slogan'      => 'De la DATA mais autrement',
);