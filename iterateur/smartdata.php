<?php
/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2019                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Gestion de l'itérateur SMARTDATA
 *
 * @package SPIP\Plugin\Smartdata\Iterateur
 *

 * Ce smart itérator prend en compte le critère {10,5} c'est à dire 'limit'
 * et s'assure que la boucle SPIP ne prend pas AUSSI en compte ce critère, par la suite
 *
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('iterateur/data');


/**
 * Créer une boucle sur un itérateur SMARTDATA
 *
 * Annonce au compilateur les "champs" disponibles,
 * c'est à dire 'cle' et 'valeur'.
 *
 * @param Boucle $b
 *     Description de la boucle
 * @return Boucle
 *     Description de la boucle complétée des champs
 */
function iterateur_SMARTDATA_dist($b) {
	$b = iterateur_DATA_dist($b);
	$b->iterateur = 'SMARTDATA';    // designe la classe d'iterateur
	$b->type_requete='DATA';        // nécessaire correction sinon l'itérateur n'est pas appelé par la suite
	return $b;
}

/**
 * Itérateur SMARTDATA
 *
 * Pour itérer sur des données quelconques (transformables en tableau)
 */
class IterateurSMARTDATA extends IterateurDATA {

	/**
	 * Constructeur
	 *
	 * @param  $command : le smart itérateur gère certain des critères
	 * et renvoie un tableau de commande modifié (a priori allégé car ne contenant que ce que doit encore traiter la boucle SPIP)
	 *
	 * @param array $info
	 */
	public function __construct(&$command, $info = array()) {
		parent::__construct($command, $info);

		// MODIF : on repart avec la valeur de command renvoyée par la fonction _to_smart_array
		$command = $this->command;
	}


	/**
	 * MODIF : on surcharge la méthode select_source
	 *
	 * Aller chercher les donnees de la boucle SMARTDATA
	 * depuis une source
	 * {source format, [URL], [arg2]...}
	 *
	 * Version SMARTDATA aménagée
	 */
	protected function select_source() {
		# un peu crado : avant de charger le cache il faut charger
		# les class indispensables, sinon PHP ne saura pas gerer
		# l'objet en cache ; cf plugins/icalendar
		# perf : pas de fonction table_to_array ! (table est deja un array)
		if (isset($this->command['sourcemode'])
			and !in_array($this->command['sourcemode'], array('table', 'array', 'tableau'))
		) {
			charger_fonction($this->command['sourcemode'] . '_to_array', 'inc', true);
		}

		# le premier argument peut etre un array, une URL etc.
		$src = $this->command['source'][0];

		# avons-nous un cache dispo ?
		$cle = null;
		if (is_string($src)) {
			$cle = 'datasource_' . md5($this->command['sourcemode'] . ':' . var_export($this->command['source'], true));
		}

		$cache = $this->cache_get($cle);
		if (isset($this->command['datacache'])) {
			$ttl = intval($this->command['datacache']);
		}
		if ($cache
			and ($cache['time'] + (isset($ttl) ? $ttl : $cache['ttl'])
				> time())
			and !(_request('var_mode') === 'recalcul'
				and include_spip('inc/autoriser')
				and autoriser('recalcul')
			)
		) {
			$this->tableau = $cache['data'];
		} else {
			try {
				# dommage que ca ne soit pas une option de yql_to_array...
				if ($this->command['sourcemode'] == 'yql') {
					if (!isset($ttl)) {
						$ttl = 3600;
					}
				}

				if (isset($this->command['sourcemode'])
					and in_array($this->command['sourcemode'],
						array('table', 'array', 'tableau'))
				) {
					if (is_array($a = $src)
						or (is_string($a)
							and $a = str_replace('&quot;', '"', $a) # fragile!
							and is_array($a = @unserialize($a)))
					) {
						$this->tableau = $a;
					}
				} else {
					if (tester_url_absolue($src)) {
						include_spip('inc/distant');
						$u = recuperer_page($src, false, false, _DATA_SOURCE_MAX_SIZE);
						if (!$u) {
							throw new Exception("404");
						}
						if (!isset($ttl)) {
							$ttl = 24 * 3600;
						}
					} else {
						if (@is_dir($src)) {
							$u = $src;
							if (!isset($ttl)) {
								$ttl = 10;
							}
						} else {
							if (@is_readable($src) && @is_file($src)) {
								$u = spip_file_get_contents($src);
								if (!isset($ttl)) {
									$ttl = 10;
								}
							} else {
								$u = $src;
								if (!isset($ttl)) {
									$ttl = 10;
								}
							}
						}
					}
					// Début des modifications
					// MODIF S'il existe une fonction smart, on l'appelle en priorité
					if (!$this->err) {
						$args = $this->command['source'];
						$args[0] = $u;

						// Optimise les chargements :
						// les fonctions _to_array et _to_smart_array peuvent être définies dans le fichier _to_array
						$gdatafunc = charger_fonction($this->command['sourcemode'] . '_to_array', 'inc', true);
						if ($gsmartfunc = charger_fonction($this->command['sourcemode'] . '_to_smart_array', 'inc', true)) {
							// On lui passe la liste des critères en 1er argument
							array_unshift($args, $this->command);
							// Et elle renvoie un couple (tableau de résultat, liste des critères modifiées)
							// car il semble impossible avec les php récents de passer un argument par référence avec call_user_func_array (.)
							list ($a, $smart_command) = call_user_func_array($gsmartfunc, $args);
							if (is_array($a)) {
								$this->tableau = $a;
							}
							if (is_array($smart_command))
								$this->command = $smart_command;
						}
						// Sinon rien de smart et on utilise la fonction _to_array pour DATA legacy
						elseif ($gdatafunc) {
							$a = call_user_func_array($gdatafunc, $args);
							if (is_array($a)) {
								$this->tableau = $a;
							}
						}
					}
					// MODIF : Fin
				}

				if (!is_array($this->tableau)) {
					$this->err = true;
				}

				if (!$this->err and isset($ttl) and $ttl > 0) {
					$this->cache_set($cle, $ttl);
				}

			} catch (Exception $e) {
				$e = $e->getMessage();
				$err = sprintf("[%s, %s] $e",
					$src,
					$this->command['sourcemode']);
				erreur_squelette(array($err, array()));
				$this->err = true;
			}
		}

		# en cas d'erreur, utiliser le cache si encore dispo
		if ($this->err
			and $cache
		) {
			$this->tableau = $cache['data'];
			$this->err = false;
		}
	}

}
